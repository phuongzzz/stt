//
//  PlayViewController.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/18/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import UIKit
import AVFoundation
class PlayViewController : UIViewController, AVAudioPlayerDelegate{
    
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var content: UITextView!
    var record : Record?
    var player: AVAudioPlayer?
    var audioFileUrl : URL?
    var displaylink: CADisplayLink!
    var index = 0
    var attributedString : NSMutableAttributedString?
    var timer: Timer?
    
    enum State {
        case idle, play
    }
    
    var state = State.idle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setSessionPlayAndRecord()
        fixBundleImagesLoading()
        content.isEditable = false
        if let record = record {
            audioFileUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(record.id! + ".wav")
            for content in record.contents {
                self.content.text.append(content.script + " ")
            }
            displaylink = CADisplayLink(target: self, selector: #selector(trackAudio))
            displaylink.add(to: RunLoop.main, forMode: RunLoop.Mode.common)
            attributedString = NSMutableAttributedString(string: self.content.text)
            attributedString!.addAttribute(NSAttributedString.Key.font, value: UIFont.boldSystemFont(ofSize: 18), range: (self.content.text as NSString).range(of: self.content.text))
            self.content.attributedText = attributedString
        }
    }
    
    func setSessionPlayAndRecord() {
        let session = AVAudioSession.sharedInstance()
        do {
            try session.setCategory(AVAudioSession.Category.playAndRecord)
        } catch {
            print("could not set session category")
            print(error.localizedDescription)
        }
        do {
            try session.setActive(true)
        } catch {
            print("could not make session active")
            print(error.localizedDescription)
        }
        do {
            try session.overrideOutputAudioPort(AVAudioSession.PortOverride.speaker)
        } catch {
            print("could not set output to speaker")
            print(error.localizedDescription)
        }
    }
   

    
    @objc func trackAudio() {
        if let player = self.player{
            let currentTime = player.currentTime
            let minutes = Int(currentTime / 60)
            let seconds = Int(currentTime.truncatingRemainder(dividingBy: 60))
            let totalSeconds = minutes > 0 ? minutes * 60 + seconds : seconds
            updateTimeLabel(String(format: "%02d:%02d", minutes, seconds))
            let startTime = Float(record!.contents[index].start_time)!
            let endTime = Float(record!.contents[index].end_time)!
            print("Total Seconds:%s",totalSeconds)
            print("Start Time:%s",startTime)
            print("End Time:%s",endTime)
            if index < (record!.contents.count - 1){
                if  totalSeconds == Int(startTime){
                    attributedString!.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.white, range: (self.content.text as NSString).range(of: self.content.text))
                    attributedString!.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.yellow, range: (self.content.text as NSString).range(of: record!.contents[index].script))
                    self.content.attributedText = attributedString
                    self.autoScroll(range: (self.content.text as NSString).range(of: record!.contents[index].script))
                }
                else if  totalSeconds == Int(endTime){
                    index += 1
                }
            }
            else{
                attributedString!.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.white, range: (self.content.text as NSString).range(of: self.content.text))
                attributedString!.addAttribute(NSAttributedString.Key.backgroundColor, value: UIColor.yellow, range: (self.content.text as NSString).range(of: record!.contents[index].script))
                self.content.attributedText = attributedString
                self.autoScroll(range: (self.content.text as NSString).range(of: record!.contents[index].script))
            }
        }
    }
    
    fileprivate func fixBundleImagesLoading() {
        playBtn.setImage(UIImage(named: "RecorderResources.bundle/ic_play_btn.png"), for: .normal)
        playBtn.setImage(UIImage(named: "RecorderResources.bundle/ic_pause_btn.png"), for: .selected)
    }
    
    func playRecordedMessage() {
        if player == nil {
            do {
                player = try AVAudioPlayer(contentsOf: audioFileUrl!)
                player?.delegate = self
                player?.isMeteringEnabled = true
                player?.volume = 100
                player?.prepareToPlay()
            } catch let e {
                print("PlayViewController: create AVAudioPlayer error", e)
            }
        }
        
        if let player = player {
            player.stop()
            player.play()
            if player.isPlaying {
                state = .play
                playBtn.isSelected = true
            }
        }
    }
    
    func pausePlayback() {
        playBtn.isSelected = false
        player?.stop()
        player?.currentTime = 0
        player = nil
        state = .idle
        index = 0
        timeLabel.text = "00:00"
    }
    
    fileprivate func updateTimeLabel(_ timeFormatted: String) {
        ui {[weak self] in
            self?.timeLabel.text = timeFormatted
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        pausePlayback()
       //releaseDisplayLink()
    }
    
    fileprivate func releaseDisplayLink() {
        displaylink.isPaused = true
        displaylink.remove(from: RunLoop.main, forMode: RunLoop.Mode.common)
        displaylink.invalidate()
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        pausePlayback()
    }
    
    @IBAction func playBtnAction(_ sender: Any) {
        if state == .play {
            pausePlayback()
        } else {
            playRecordedMessage()
        }
    }
    
    func autoScroll(range : NSRange){
        self.content.scrollRangeToVisible(range)
    }
}
