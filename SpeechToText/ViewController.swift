//
//  ViewController.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/15/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    private var records:Results<Record>?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return records?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ModelTableViewCell,
            let record = records?[indexPath.row] else {
                return ModelTableViewCell(frame: .zero)
        }
        cell.configureWith(record)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) is ModelTableViewCell
            else {
                return
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Confirm delete", message: "Are you sure you want to delete this item?", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: UIAlertAction.Style.destructive){(action:UIAlertAction) in
                self.deleteRecord(index: indexPath.row)
                self.deleteRecordFile(id: self.records![indexPath.row].id!)
            })
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "Play Audio") {
            let playViewController: PlayViewController = segue.destination as! PlayViewController
            if let indexPath = self.tableView.indexPathForSelectedRow {
                playViewController.record = records?[indexPath.row]
            }
        }
    }
    
    func deleteRecordFile(id : String){
        if !id.isEmpty{
            let fileManager = FileManager.default
            let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(id + ".wav")
            let filePathName = url.path
            do {
                try fileManager.removeItem(atPath: filePathName)
                
            } catch {
                print("Could not delete file: \(error)")
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
    
    func getDataFromDB()
    {
        DispatchQueue.main.async {
            self.records = Record.getAll()
            self.tableView.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getDataFromDB()
    }
    
    func setupTableView()
    {
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.allowsMultipleSelectionDuringEditing = false;
    }
    
    func deleteRecord(index : Int){
        DispatchQueue.main.async {
            autoreleasepool {
                let realm = try! Realm()
                try! realm.write {
                    realm.delete(self.records![index])
                    let alert = UIAlertController(title: "", message: "Completed", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel){(action:UIAlertAction) in
                        self.getDataFromDB()
                    })
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
}

