//
//  ModelTableViewCell.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/18/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import UIKit
class ModelTableViewCell : UITableViewCell{
    @IBOutlet weak var content: UILabel!
    @IBOutlet weak var date: UILabel!
    private var record: Record?
    func configureWith(_ record: Record) {
        self.record = record
        self.date.text = record.date
        self.content.text = ""
        for content in record.contents{
            self.content.text?.append(content.script + " ")
        }
    }
}
