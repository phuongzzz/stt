//
//  StringExt.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/15/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import Foundation
extension String {
func base64Encoded() -> String? {
    return data(using: .utf8)?.base64EncodedString()
}

func base64Decoded() -> String? {
    var st = self;
    if (self.count % 4 <= 2){
        st += String(repeating: "=", count: (self.count % 4))
    }
    guard let data = Data(base64Encoded: st) else { return nil }
    return String(data: data, encoding: .utf8)
    }
}
