//
//  RecorderViewController.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/15/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import UIKit
import Starscream
import AVFoundation
import RealmSwift

class RecorderViewController: UIViewController, WebSocketDelegate {
    
    @IBOutlet weak var mainText: UITextView!
    @IBOutlet weak var streamingText: UITextView!
    @IBOutlet weak var recordBtnBack: RedRecordBtnBack!
    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var audioPlot: SoundWaveVisualizer!
    @IBOutlet weak var titleLabel: UILabel!
    private var uuid = ""
    let domain = "172.16.18.98:3000"
    var contents = [Content]()
    
    let engine = AVAudioEngine()
    let outputFile = AVAudioFile()
    
    let conversionQueue = DispatchQueue(label: "conversionQueue")
    
    var audioId = 0
    var content = ""
    var socket : WebSocket?
    
    private var bufferSize = 3200
    private var sampleRate: Double = 16000
    private var currentDate = ""
    private var isRecordSaved = false
    
    enum State {
        case idle, record
    }
    var state = State.idle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainText.isEditable = false
        streamingText.isEditable = false
        audioPlot.backgroundColor = UIColor.clear
        audioPlot.updateWithPowerLevel(0.0)
        streamingText.isScrollEnabled = true
        mainText.isScrollEnabled = true
        setupNavigationBar()
    }
    
    func setupNavigationBar(){
        let imageView = UIImageView()
        imageView.backgroundColor = UIColor.clear
        imageView.frame = CGRect(x:0,y:0,width:2*(self.navigationController?.navigationBar.bounds.height)!,height:(self.navigationController?.navigationBar.bounds.height)!)
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(backToParent))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
        imageView.tag = 1
        self.navigationController?.navigationBar.addSubview(imageView)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Save", style: .done, target: self, action: #selector(self.saveRecord(sender:)))
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        finishRecording(success: isRecordSaved)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupAudioSession()
    }
    
    fileprivate func setupAudioSession() {
        audioPlot.backgroundColor = UIColor.clear
        let audioSession = AVAudioSession.sharedInstance()
        audioSession.requestRecordPermission {[weak self] granted in
            guard let this = self else { return }
            DispatchQueue.main.async {
                this.recordBtn.isEnabled = granted
                this.recordBtnBack.isEnabled = granted
                this.titleLabel.isHidden = granted ? true : false
                if !granted {
                    this.titleLabel.text = L("Enable microphone access\nfrom system preferences")
                }
            }
        }
        do {
            try audioSession.setPreferredSampleRate(self.sampleRate)
            try audioSession.setCategory(.playAndRecord, mode: .measurement, options: .duckOthers)
            try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
        } catch {
            print("VoiceRecordView: setup AVAudioSession error", error)
        }
    }
    
    func startRecording(){
        print("Start Recording...")
        self.deleteRecordFile()
        self.resetRecordData()
        self.state = .record
        self.saveCurrentDate()
        self.uuid = NSUUID().uuidString.lowercased()
        self.prepareSocket(self.uuid)
        let inputNode = self.engine.inputNode
        let inputFormat = inputNode.outputFormat(forBus: 0)
        let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.uuid + ".caf")
        let file: AVAudioFile
        do {
            file = try AVAudioFile(forWriting: url, settings: self.engine.inputNode.outputFormat(forBus: 0).settings)
        } catch {
            print("Error: \(error)")
            return
        }
        inputNode.installTap(onBus: 0, bufferSize: AVAudioFrameCount(self.bufferSize), format: inputFormat) { (buffer: AVAudioPCMBuffer, when: AVAudioTime) in
            self.conversionQueue.async {
                if let socket = self.socket {
                    if socket.isConnected {
                        self.updateVisualizer(buffer: buffer)
                        self.audioId += 1
                        let request = Request()
                        request.session_id = self.uuid
                        request.audio_id = self.audioId
                        let converBuffer = self.convertBufferTo16Khz(input: buffer)
                        let data = self.audioBufferToData(audioBuffer: converBuffer!)
                        request.data_file = data.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
                        let jsonEncoder = JSONEncoder()
                        let jsonData = try! jsonEncoder.encode(request)
                        let json = String(data: jsonData, encoding: String.Encoding.utf8)
                        self.sendMessage(message: json!)
                        do {
                            try file.write(from: buffer)
                        } catch {
                            print("Error: \(error)")
                        }
                    }
                    else {
                        print("Socket Disconnecte")
                    }
                }
            }
        }
        self.engine.prepare()
        do {
            try self.engine.start()
        }
        catch {
            print(error.localizedDescription)
        }
    }
    
    
    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    func finishRecording(success: Bool) {
        if (success){
            state = .idle
            if engine.isRunning{
                engine.stop()
                engine.inputNode.removeTap(onBus: 0)
            }
            closeSocketConnection()
            self.audioId = 0
            self.navigationItem.rightBarButtonItem?.isEnabled = self.contents.isEmpty ? false : true
        }
        else{
            deleteRecordFile()
        }
    }
    
    func deleteRecordFile(){
        if !self.uuid.isEmpty{
            let fileManager = FileManager.default
            let url = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.uuid + ".caf")
            let filePathName = url.path
            do {
                try fileManager.removeItem(atPath: filePathName)
                
            } catch {
                print("Could not delete file: \(error)")
            }
        }
    }
    
    func prepareSocket(_ sessionId:String )
    {
        if let url = URL(string: "ws://\(domain)/client/ws/speech?sessionId=\(sessionId)"){
            self.socket = WebSocket(url: url)
        }
        self.socket?.connect()
        self.socket?.delegate = self
    }
    func closeSocketConnection(){
        if self.socket != nil {
            if self.socket!.isConnected
            {
                self.socket!.disconnect()
                self.socket!.delegate = nil
            }
        }
    }
    
    func websocketDidConnect(socket: WebSocketClient) {
        print("WebSocket Connected")
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("WebSocket Disconnected")
        print("Connect error = \(String(describing: error))")
        recordBtnBack.animateStop()
        finishRecording(success: false)
        let alert = UIAlertController(title: "Error", message: "Socket Disconnected", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("WebSocket Received Message")
        print("Received message = " + text)
        let jsonData = Data(text.utf8)
        let decoder = JSONDecoder()
        do {
            let response = try decoder.decode(Response.self, from: jsonData)
            if(response.script != nil){
                if(response.status == 1)
                {
                    self.mainText.text.append(self.streamingText.text)
                    self.contents.append(Content(input : response))
                    self.content = ""
                    self.streamingText.text = ""
                    self.autoScroll(view: self.mainText)
                }
                else {
                    self.content = response.script! + " "
                    self.streamingText.text = self.content
                    self.autoScroll(view: self.streamingText)
                }
            }
            print(response)
        } catch {
            print(error.localizedDescription)
        }
    }
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("WebSocket Received Data")
    }
    func sendMessage(message:String)
    {
        print("Sent Message = \(message)")
        socket?.write(string: message)
    }
    
    func convertBufferTo16Khz(input:AVAudioPCMBuffer) -> AVAudioPCMBuffer?
    {
        let targetFormat = AVAudioFormat(commonFormat: AVAudioCommonFormat.pcmFormatInt16, sampleRate: 16000, channels: 1, interleaved: true)
        let convertBuffer = AVAudioPCMBuffer(pcmFormat: targetFormat!, frameCapacity: input.frameCapacity)
        convertBuffer?.frameLength = convertBuffer!.frameCapacity
        let converter = AVAudioConverter(from: input.format, to: convertBuffer!.format)
        converter?.sampleRateConverterAlgorithm = AVSampleRateConverterAlgorithm_Normal
        converter?.sampleRateConverterQuality = .max
        guard let output = convertBuffer else { return nil }
        do {
            try converter!.convert(to: output, from: input)
            return output
        } catch let error {
            print("Conversion error: \(error)")
            return nil
        }
    }
    
    func audioBufferToData(audioBuffer: AVAudioPCMBuffer) -> Data {
        let channelCount = 1
        let bufferLength = (audioBuffer.frameCapacity * audioBuffer.format.streamDescription.pointee.mBytesPerFrame)
        let channels = UnsafeBufferPointer(start: audioBuffer.int16ChannelData, count: channelCount)
        let data = Data(bytes: channels[0], count: Int(bufferLength))
        print("\(#file) > \(#function) > Values: bufferLength: \(bufferLength), channels: \(channels), data: \(data)")
        return data
    }
    
    func audioBufferToBytes(audioBuffer: AVAudioPCMBuffer) -> [UInt8] {
        let srcLeft = audioBuffer.floatChannelData![0]
        let bytesPerFrame = audioBuffer.format.streamDescription.pointee.mBytesPerFrame
        let numBytes = Int(bytesPerFrame * audioBuffer.frameLength)
        
        // initialize bytes by 0
        var audioByteArray = [UInt8](repeating: 0, count: numBytes)
        
        srcLeft.withMemoryRebound(to: UInt8.self, capacity: numBytes) { srcByteData in
            audioByteArray.withUnsafeMutableBufferPointer {
                $0.baseAddress!.initialize(from: srcByteData, count: numBytes)
            }
        }
        return audioByteArray
    }
    
    @IBAction func recordBtnAction(_ sender: UIButton?) {
        switch state {
        case .idle:
            if(contents.isEmpty){
                recordBtnBack.animateRecord()
                sender?.isSelected = true
                startRecording()
            }
            else {
                let alert = UIAlertController(title: "Confirm!!!", message: "You have data was not saved. Are you sure you want to create new record?", preferredStyle: .actionSheet)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.destructive){(action:UIAlertAction) in
                    self.recordBtnBack.animateRecord()
                    sender?.isSelected = true
                    self.startRecording()
                })
                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            
        case .record:
            recordBtnBack.animateStop()
            sender?.isSelected = false
            finishRecording(success: true)
            
        }
        
    }
    func resetRecordData() {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.content = ""
        self.mainText.text = ""
        self.streamingText.text = ""
        self.contents.removeAll()
        self.uuid = ""
    }
    func normalizedPower(_ decibels: Float) -> Float {
        if (decibels < -60.0 || decibels == 0.0) {
            return 0.0
        }
        let para1 = pow(10.0, 0.05 * decibels) - pow(10.0, 0.05 * -60.0)
        let para2 = 1.0 / (1.0 - pow(10.0, 0.05 * -60.0))
        let para3 = 1.0 / 2.0
        return powf(para1 * Float(para2), Float(para3))
    }
    
    func updateVisualizer(buffer : AVAudioPCMBuffer ) {
        DispatchQueue.main.async {
            var normalizedValue: Float = 0.0
            if self.state == .record {
                let channelData = buffer.floatChannelData
                let channelDataValue = channelData!.pointee
                let channelDataValueArray = stride(from: 0,
                                                   to: Int(buffer.frameLength),
                                                   by: buffer.stride).map{ channelDataValue[$0] }
                let para1 = channelDataValueArray.map{ $0 * $0 }.reduce(0, +) / Float(buffer.frameLength)
                let rms = sqrt(para1)
                let avgPower = 20 * log10(rms)
                normalizedValue = self.normalizedPower(avgPower)
            }
            
            self.audioPlot.updateWithPowerLevel(normalizedValue)
        }
    }
    
    @objc func saveRecord(sender: UIBarButtonItem) {
        let record = Record(self.uuid)
        for content in self.contents {
            record.contents.append(content)
        }
        record.date = self.currentDate
        self.conversionQueue.async {
            autoreleasepool {
                let realm = try! Realm()
                try! realm.write {
                    realm.add(record)
                    self.convertAudioFile()
                    self.isRecordSaved = true
                    DispatchQueue.main.sync {
                        self.resetRecordData()
                        let alert = UIAlertController(title: "", message: "Completed", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
    
    func saveCurrentDate(){
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd HH:mm"
        currentDate = formatter.string(from: currentDateTime)
    }
    
    @objc func backToParent() {
        if !contents.isEmpty {
            confirmIfHaveOldData(message: "You have data was not saved. Are you sure you want to quit???", title: "Quit")
        }
        else {
            self.navigationController?.popViewController(animated: true)
            print("done")
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        for view in (self.navigationController?.navigationBar.subviews)!{
            if view.tag == 1 {
                view.removeFromSuperview()
            }
        }
    }
    
    func convertAudioFile() {
        if !self.uuid.isEmpty{
            let input = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.uuid + ".caf")
            let output = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!.appendingPathComponent(self.uuid + ".wav")
            var options = AKConverter.Options.init()
            options.format = "wav"
            options.sampleRate = 8000
            options.bitRate = 16
            options.channels = 1
            options.bitDepth = 16
            options.eraseFile = true
            options.isInterleaved = true
            AKConverter.init(inputURL: input, outputURL: output, options: options).start()
            deleteRecordFile()
        }
    }
    
    func confirmIfHaveOldData(message : String, title : String) {
        let alert = UIAlertController(title: "Alert!!!", message: message, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: title, style: UIAlertAction.Style.destructive){(action:UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    func autoScroll(view: UITextView){
        let range = NSMakeRange(view.text.count - 1, 0)
        view.scrollRangeToVisible(range)
    }
}

