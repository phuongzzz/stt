//
//  RecordExt.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/18/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import RealmSwift
extension Record {
    static func add(time: String, in realm: Realm = try! Realm()) -> Record {
        let record = Record(time)
        try! realm.write {
            realm.add(record)
        }
        return record
    }
}
