//
//  Request.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/15/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import Foundation
class Request : Codable{
    var audio_id:Int?
    var session_id:String?
    var data_file:String?
    var audio_length = 10
}
