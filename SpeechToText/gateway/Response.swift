//
//  Response.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/15/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import Foundation
class Response : Codable{
    var script:String?
    var start_time:String?
    var end_time:String?
    var index:Int?
    var status:Int?
}
