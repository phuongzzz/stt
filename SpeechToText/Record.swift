//
//  Record.swift
//  SpeechToText
//
//  Created by Thế Anh Nguyễn on 11/18/19.
//  Copyright © 2019 Rikkei. All rights reserved.
//

import RealmSwift
import Foundation
@objcMembers class Record : Object {
    enum Property: String {
        case id
    }
    @objc dynamic var id:String?
    override class func primaryKey() -> String? {
        return Record.Property.id.rawValue
    }
    @objc dynamic var date:String?
    var contents = List<Content>()
    convenience init(_ id: String) {
        self.init()
        self.id = id
    }
    
    static func getAll(in realm: Realm = try! Realm()) -> Results<Record> {
        return realm.objects(Record.self)
    }
}

@objcMembers class Content : Object {
    @objc dynamic var script = ""
    @objc dynamic var start_time = ""
    @objc dynamic var end_time = ""
    @objc dynamic var status = 0
    @objc dynamic var index = 0
    
    convenience init(input : Response) {
        self.init()
        self.script = input.script ?? ""
        self.start_time = input.start_time ?? ""
        self.end_time = input.end_time ?? ""
        self.status = input.status ?? 0
        self.index = input.index ?? 0
    }
}
